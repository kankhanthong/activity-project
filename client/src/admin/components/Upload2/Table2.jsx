import React, { useState, useEffect } from "react";
import axios from "axios";
import { DataGrid } from "@mui/x-data-grid";
import { Box, Checkbox } from "@mui/material";
import { Accordion, AccordionSummary, AccordionDetails } from "@mui/material";
import ExpandMoreIcon from "@mui/icons-material/ExpandMore";
import { formatDate, range, dateToUint32 } from "./Fx";

function Table2() {
  const [activity, setActivity] = useState([]);
  const [student, setStudent] = useState([]);
  const [loading, setLoading] = useState(true);
  const [checkAll, setCheckAll] = useState({}); // Store checkAll state for each act_ID
  const [checkedItems, setCheckedItems] = useState({}); // Store checked items

  const fetchData = async () => {
    try {
      const responseActivity = await axios.get(`/api/actByStatus`);
      setActivity(responseActivity.data);

      const responseStudent = await axios.get(`/api/par`);
      setStudent(responseStudent.data);
    } catch (error) {
      console.log(error);
    } finally {
      setLoading(false);
    }
  };

  useEffect(() => {
    fetchData();
  }, []);

  // Check if all students are checked for each activity
  useEffect(() => {
    activity.forEach((act) => {
      const actID = act.act_ID;
      const days = range(act.act_dateStart, act.act_dateEnd);
      const allChecked = student.every((s) =>
        days.every((d) =>
          checkedItems[actID]?.students[s.std_ID]?.includes(dateToUint32(d))
        )
      );
      setCheckAll((prev) => ({ ...prev, [actID]: allChecked })); // Update checkAll for each act_ID
    });
  }, [checkedItems, activity, student]);

  const handleCheckAllChange = (actID, checked) => {
    const act = activity.find((a) => a.act_ID === actID);
    const days = range(act.act_dateStart, act.act_dateEnd);
    const dateUints = days.map(dateToUint32);

    setCheckedItems((prev) => {
      const updated = { ...prev };

      if (checked) {
        // If checking all, create entries for all students
        updated[actID] = {
          act_title: act.act_title,
          login_id: [],
          students: {},
        };

        student.forEach((s) => {
          if (s.act_ID === actID) {
            updated[actID].students[s.std_ID] = dateUints;
            if (!updated[actID].login_id.includes(s.login_ID)) {
              updated[actID].login_id.push(s.login_ID);
            }
          }
        });
      } else {
        // If unchecking all, just clear the students
        if (updated[actID]) {
          Object.keys(updated[actID].students).forEach((stdID) => {
            delete updated[actID].students[stdID];
          });
        }
      }

      console.log("Updated checkedItems:", updated);
      return updated;
    });

    setCheckAll((prev) => ({ ...prev, [actID]: checked }));
  };

  const handleCheckAllStudentChange = (actID, login_id, stdID, checked) => {
    const act = activity.find((a) => a.act_ID === actID);
    const days = range(act.act_dateStart, act.act_dateEnd);
    const dateUints = days.map(dateToUint32);

    setCheckedItems((prev) => {
      const updated = { ...prev };
      if (!updated[actID]) {
        updated[actID] = {
          act_title: act.act_title,
          students: {},
          login_id: [],
        };
      }

      if (checked) {
        updated[actID].students[stdID] = dateUints;
        if (!updated[actID].login_id.includes(login_id)) {
          updated[actID].login_id.push(login_id);
        }
      } else {
        delete updated[actID].students[stdID];
        updated[actID].login_id = updated[actID].login_id.filter(
          (id) => id !== login_id
        );
      }

      if (Object.keys(updated[actID].students).length === 0) {
        delete updated[actID];
      }

      console.log("Updated checkedItems:", updated);
      return updated;
    });
  };

  const handleCheckboxChange = (actID, login_id, stdID, date, checked) => {
    setCheckedItems((prev) => {
      const updated = { ...prev };
      if (!updated[actID]) {
        updated[actID] = {
          act_title: activity.find((a) => a.act_ID === actID)?.act_title || "",
          students: {},
          login_id: [],
        };
      }
      if (!updated[actID].students[stdID]) {
        updated[actID].students[stdID] = [];
      }

      if (checked) {
        updated[actID].students[stdID] = [
          ...new Set([...updated[actID].students[stdID], date]),
        ];
        if (!updated[actID].login_id.includes(login_id)) {
          updated[actID].login_id.push(login_id);
        }
      } else {
        updated[actID].students[stdID] = updated[actID].students[stdID].filter(
          (d) => d !== date
        );
        if (updated[actID].students[stdID].length === 0) {
          delete updated[actID].students[stdID];
        }
      }

      if (Object.keys(updated[actID].students).length === 0) {
        delete updated[actID];
      }

      console.log("Updated checkedItems:", updated);
      return updated;
    });
  };

  console.log(checkedItems);

  if (loading) {
    return <div>Loading...</div>;
  }

  return (
    <div>
      {activity.map((act) => {
        const days = range(act.act_dateStart, act.act_dateEnd);
        const filteredStudents = student.filter((s) => s.act_ID === act.act_ID);
        return (
          <div
            key={act.act_ID}
            className="mb-10 container mx-auto md:px-20 py-40"
            style={{ marginBottom: "20px" }}
          >
            <div className="flex flex-col items-center mb-4">
              <h1 className="text-xl font-medium">
                รายชื่อผู้ลงทะเบียนเข้าร่วมกิจกรรม
              </h1>
              <h1 className="text-3xl font-bold pt-4">{act.act_title}</h1>
            </div>
            <div className="flex justify-start">
              <Accordion
                sx={{
                  width: "100%",
                  boxShadow: "none",
                  border: "none",
                }}
              >
                <AccordionSummary
                  expandIcon={<ExpandMoreIcon />}
                  aria-controls="panel1-content"
                  id="panel1-header"
                >
                  รายละเอียดกิจกรรม
                </AccordionSummary>
                <AccordionDetails>
                  <div className="grid grid-cols-10 gap-2">
                    <h1 className="col-span-2 font-semibold">กิจกรรม:</h1>
                    <p className="col-span-8">{`${act.act_title} (${act.act_ID})`}</p>

                    <h1 className="col-span-2 font-semibold">รายละเอียด:</h1>
                    <p className="col-span-8">{act.act_desc}</p>

                    <h1 className="col-span-2 font-semibold">
                      จำนวนที่เปิดรับ:
                    </h1>
                    <p className="col-span-8">
                      {act.act_numStdReserve} / {act.act_numStd} คน
                    </p>

                    <h1 className="col-span-2 font-semibold">ระยะเวลา:</h1>
                    <p className="col-span-8">
                      {formatDate(act.act_dateStart).th} -{" "}
                      {formatDate(act.act_dateEnd).th}
                    </p>

                    <h1 className="col-span-2 font-semibold">สถานที่:</h1>
                    <p className="col-span-8">{act.act_location}</p>

                    <h1 className="col-span-2 font-semibold">
                      อาจารย์ผู้จัดกิจกรรม:
                    </h1>
                    <p className="col-span-8">
                      {act.staff_fname} {act.staff_lname}
                    </p>

                    <h1 className="col-span-2 font-semibold">
                      ช่องทางการติดต่อ:
                    </h1>
                    <p className="col-span-8">{act.staff_email}</p>
                  </div>
                </AccordionDetails>
              </Accordion>
            </div>
            <Box sx={{ height: 400, width: "100%" }}>
              {filteredStudents.length > 0 ? (
                <DataGrid
                  rows={filteredStudents.map((i) => ({
                    id: i.login_ID,
                    std_id: i.std_ID,
                    name: `${i.std_fname} ${i.std_lname}`,
                  }))}
                  columns={[
                    {
                      field: "checkAllStudent",
                      headerName: "ทั้งหมด",
                      width: 150,
                      headerAlign: "center",
                      align: "center",
                      sortable: false,
                      disableColumnMenu: true,
                      renderHeader: () => (
                        <>
                          <Checkbox
                            checked={checkAll[act.act_ID] || false}
                            onChange={(e) =>
                              handleCheckAllChange(act.act_ID, e.target.checked)
                            }
                          />
                          <span>ทั้งหมด</span>
                        </>
                      ),
                      renderCell: (params) => {
                        const isChecked =
                          !!checkedItems[act.act_ID]?.students[
                            params.row.std_id
                          ];

                        return (
                          <Checkbox
                            checked={isChecked}
                            onChange={(e) =>
                              handleCheckAllStudentChange(
                                act.act_ID,
                                params.row.id,
                                params.row.std_id,
                                e.target.checked
                              )
                            }
                          />
                        );
                      },
                    },
                    { field: "std_id", headerName: "รหัสนักศึกษา", width: 150 },
                    { field: "name", headerName: "ชื่อ - นามสกุล", width: 250 },
                    ...days.map((day) => ({
                      field: day,
                      headerName: String(formatDate(day).th).substring(0, 5), // Ensure it's a string
                      width: 100,
                      renderCell: (params) => {
                        const isChecked = !!checkedItems[act.act_ID]?.students[
                          params.row.std_id
                        ]?.includes(dateToUint32(day));

                        return (
                          <Checkbox
                            checked={isChecked}
                            onChange={(e) =>
                              handleCheckboxChange(
                                act.act_ID,
                                params.row.id,
                                params.row.std_id,
                                dateToUint32(day),
                                e.target.checked
                              )
                            }
                          />
                        );
                      },
                    })),
                  ]}
                />
              ) : (
                <p>ไม่มีนักศึกษาที่ลงทะเบียนในกิจกรรมนี้</p>
              )}
            </Box>
          </div>
        );
      })}
    </div>
  );
}

export default Table2;
