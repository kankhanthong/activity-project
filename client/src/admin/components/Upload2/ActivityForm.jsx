import { useState } from "react";
import Box from "@mui/material/Box";
import { DataGrid } from "@mui/x-data-grid";
import { range } from "./Fx";

const ActivityForm = ({ activity = {}, student = [] }) => {
  const [selectStdId, setSelectStdId] = useState([]);
  const [selectedDays, setSelectedDays] = useState({}); // เก็บวันที่ถูกเลือกสำหรับแต่ละนักศึกษา
  const days = range(activity.act_dateStart, activity.act_dateEnd);

  // สร้างคอลัมน์พื้นฐาน
  const columns = [
    {
      field: "id",
      headerName: "รหัสนักศึกษา",
      width: 150,
    },
    {
      field: "name",
      headerName: "ชื่อ-นามสกุล",
      width: 200,
    },
    // สร้างคอลัมน์สำหรับแต่ละวัน
    ...days.map((day) => ({
      field: day,
      headerName: day, // ใช้วันที่เป็น header
      width: 120,
      renderCell: (params) => (
        <input
          type="checkbox"
          checked={selectedDays[params.row.id]?.includes(day) || false}
          onChange={(e) => handleDaySelection(e, params.row.id, day)}
        />
      ),
    })),
  ];

  // สร้างแถวสำหรับนักศึกษา
  const rows = student.map((i) => ({
    id: i.std_ID,
    name: `${i.std_fname} ${i.std_lname}`,
  }));

  // ฟังก์ชันสำหรับจัดการการเลือกวันที่
  const handleDaySelection = (event, studentId, day) => {
    setSelectedDays((prevSelectedDays) => {
      const updatedDays = { ...prevSelectedDays };
      if (!updatedDays[studentId]) {
        updatedDays[studentId] = [];
      }
      if (event.target.checked) {
        updatedDays[studentId].push(day); // เพิ่มวันที่ที่ถูกเลือก
      } else {
        updatedDays[studentId] = updatedDays[studentId].filter(
          (d) => d !== day
        ); // เอาออกถ้าไม่ได้เลือก
      }
      return updatedDays;
    });
  };

  const handleSelectionChange = (newSelection) => {
    setSelectStdId(newSelection);
  };

  const upload = () => {
    console.log("Selected Student IDs:", selectStdId);
    console.log("Selected Days:", selectedDays);
    console.log("Activity ID:", activity.act_ID);
  };

  return (
    <Box sx={{ height: 400, width: "100%" }}>
      <DataGrid
        rows={rows}
        columns={columns}
        pageSizeOptions={[5]}
        checkboxSelection
        getRowId={(row) => row.id}
        onRowSelectionModelChange={(newSelection) =>
          handleSelectionChange(newSelection)
        }
      />
      <button onClick={upload}>click me</button>
    </Box>
  );
};

export default ActivityForm;
