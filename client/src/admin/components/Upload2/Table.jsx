import { useState, useEffect } from "react";
import Swal from "sweetalert2";
import Web3 from "web3";
import abi from "../../../components/contract/abi2.json";
import { formatDate, range, dateToUint32 } from "./Fx";
import axios from "axios";
import Box from "@mui/material/Box";
import { DataGrid } from "@mui/x-data-grid";
import Button from "@mui/material/Button";
import Checkbox from "@mui/material/Checkbox";

function Table({ activity = {}, student = [] }) {
  const [checkedItems, setCheckedItems] = useState({});
  const [checkAll, setCheckAll] = useState(false);
  const [web3, setWeb3] = useState(null);
  const [contract, setContract] = useState(null);
  console.log(activity);

  const contractAddress = import.meta.env.VITE_SMARTCONTRACT_ADDRESS;

  const days = range(activity.act_dateStart, activity.act_dateEnd);
  console.log(checkedItems);

  const handleCheckboxChange = (actID, login_id, stdID, date, checked) => {
    setCheckedItems((prev) => {
      const updated = { ...prev };
      if (!updated[actID]) {
        updated[actID] = {
          act_title: activity.act_title,
          students: {},
          login_id: [],
        };
      }
      if (!updated[actID].students[stdID]) {
        updated[actID].students[stdID] = [];
      }

      if (checked) {
        updated[actID].students[stdID] = [
          ...new Set([...updated[actID].students[stdID], date]),
        ];
        if (!updated[actID].login_id.includes(login_id)) {
          updated[actID].login_id.push(login_id);
        }
      } else {
        updated[actID].students[stdID] = updated[actID].students[stdID].filter(
          (d) => d !== date
        );
        if (updated[actID].students[stdID].length === 0) {
          delete updated[actID].students[stdID];
          delete updated[actID].login_id;
        }
      }

      // Clean up if there are no students left
      if (Object.keys(updated[actID].students).length === 0) {
        delete updated[actID];
      }

      return updated;
    });
  };

  const handleCheckAllChange = (checked) => {
    setCheckAll(checked);

    setCheckedItems((prev) => {
      const updated = { ...prev };

      // Initialize the activity if not already present
      if (!updated[activity.act_ID]) {
        updated[activity.act_ID] = {
          act_title: activity.act_title,
          students: {},
          login_id: [],
        };
      }

      // Iterate through each student and either add or remove their participation data
      student.forEach((s) => {
        if (checked) {
          // Add all days for each student
          updated[activity.act_ID].students[s.std_ID] = days.map((d) =>
            dateToUint32(d)
          );

          // Ensure the login_id is added once for each student
          if (!updated[activity.act_ID].login_id.includes(s.login_ID)) {
            updated[activity.act_ID].login_id.push(s.login_ID);
          }
        } else {
          // Uncheck - remove student data and login_id
          delete updated[activity.act_ID].students[s.std_ID];
        }
      });

      // Clean up if no students are left
      if (Object.keys(updated[activity.act_ID].students).length === 0) {
        delete updated[activity.act_ID];
      }

      return updated;
    });
  };

  const handleCheckAllStudentChange = (login_id, stdID, checked) => {
    setCheckedItems((prev) => {
      const updated = { ...prev };

      // Initialize act_ID if it doesn't exist
      if (!updated[activity.act_ID]) {
        updated[activity.act_ID] = {
          act_title: activity.act_title,
          students: {},
          login_id: [],
        };
      }

      if (checked) {
        // Add dates for the student and login_id
        updated[activity.act_ID].students[stdID] = days.map((d) =>
          dateToUint32(d)
        );
        if (!updated[activity.act_ID].login_id.includes(login_id)) {
          updated[activity.act_ID].login_id.push(login_id);
        }
      } else {
        // Remove the student's data and login_id
        delete updated[activity.act_ID].students[stdID];

        // Ensure login_id exists before filtering
        if (updated[activity.act_ID].login_id) {
          updated[activity.act_ID].login_id = updated[
            activity.act_ID
          ].login_id.filter((id) => id !== login_id);
        }
      }

      // Clean up if no students remain
      if (Object.keys(updated[activity.act_ID].students).length === 0) {
        delete updated[activity.act_ID];
      }

      return updated;
    });
  };

  useEffect(() => {
    const allChecked = student.every((s) =>
      days.every((d) =>
        checkedItems[activity.act_ID]?.students[s.std_ID]?.includes(
          dateToUint32(d)
        )
      )
    );
    setCheckAll(allChecked);
  }, [checkedItems, activity.act_ID, student, days]);

  const upload = async () => {
    if (!window.ethereum) {
      Swal.fire({
        icon: "error",
        title: "MetaMask Not Detected",
        text: "Please install MetaMask to proceed.",
      });
      return;
    }

    try {
      const accounts = await window.ethereum.request({
        method: "eth_requestAccounts",
      });
      const web3Instance = new Web3(window.ethereum);
      const contractInstance = new web3Instance.eth.Contract(
        abi,
        contractAddress
      );

      setWeb3(web3Instance);
      setContract(contractInstance);

      for (const actID in checkedItems) {
        const actData = checkedItems[actID];
        const studentIds = Object.keys(actData.students);
        const login_ID = actData.login_id;

        // Validate login_ID before sending
        if (!login_ID || login_ID.length === 0) {
          throw new Error("No students selected for notification");
        }

        const dayJoin = studentIds.map((stdId) => actData.students[stdId]);

        // Convert actID and studentIds to string format to avoid BigInt serialization error
        const actIDString = actID.toString(); // Convert actID to string
        const studentIdsString = studentIds.map((id) => id.toString()); // Convert each student ID to string
        const dayJoinString = dayJoin.map(
          (days) => days.map((day) => day.toString()) // Convert each day to string
        );

        const notCheckedStudents = student
          .filter((s) => !checkedItems[activity.act_ID]?.students[s.std_ID])
          .map((s) => s.std_ID);

        // Send transaction to blockchain
        const tx = await contractInstance.methods
          .createActivity(actIDString, studentIdsString, dayJoinString)
          .send({ from: accounts[0] });

        const transactionHashString = tx.transactionHash.toString();

        // Update participation status via API
        if (notCheckedStudents.length > 0) {
          await axios.put(`/api/participate`, {
            act_ID: actID,
            par_status: "ลงทะเบียนแต่ไม่เข้าร่วม",
            std_ID: notCheckedStudents,
          });
        }

        if (studentIds.length > 0) {
          await axios.put(`/api/participate`, {
            act_ID: actID,
            par_status: "เข้าร่วม",
            std_ID: studentIds,
          });
        }

        // Patch the act_transaction using the stringified transaction hash
        await axios.patch(`/api/uploadWeb3`, {
          act_ID: actID,
          act_transaction: transactionHashString,
        });

        // Send notification to students who joined
        try {
          await axios.post(`/api/upload`, {
            topic: `ยืนยันผลการเข้าร่วมกิจกรรม ${activity.act_title}`,
            description: "สามารถดูรายละเอียดการเข้าร่วมกิจกรรมได้ที่แล้ววันนี้",
            login_ID: login_ID // Ensure this is an array
          });
        } catch (notificationError) {
          console.error("Notification error:", notificationError.response?.data || notificationError.message);
          // Optionally show a warning but don't fail the whole process
          Swal.fire({
            icon: 'warning',
            title: 'Notification Warning',
            text: 'Transaction successful but notification failed to send.',
          });
        }

        Swal.fire({
          position: "top-end",
          title: `Transaction: ${transactionHashString}`,
          icon: "success",
          showConfirmButton: false,
          timer: 1500,
        });

        setTimeout(() => {
          window.location.reload();
        }, 1000);
      }
    } catch (err) {
      console.error("Error handling upload:", err);
      Swal.fire({
        icon: "error",
        title: "Upload Failed",
        text: err.response?.data?.error || err.message || "An error occurred during upload.",
      });
    }
  };

  const columns = [
    {
      field: "checkAllStudent",
      headerName: (
        <>
          <Checkbox
            checked={checkAll}
            onChange={(e) => handleCheckAllChange(e.target.checked)}
          />
          <span>ทั้งหมด</span>
        </>
      ),
      width: 150,
      headerAlign: "center",
      align: "center",
      sortable: false,
      disableColumnMenu: true,
      renderCell: (params) => {
        const allDaysChecked = days.every((d) =>
          checkedItems[activity.act_ID]?.students[params.row.std_id]?.includes(
            dateToUint32(d)
          )
        );
        return (
          <Checkbox
            checked={allDaysChecked}
            onChange={(e) =>
              handleCheckAllStudentChange(
                params.row.id, // Make sure this is the correct identifier
                params.row.std_id,
                e.target.checked
              )
            }
          />
        );
      },
    },

    {
      field: "std_id",
      headerName: "รหัสนักศึกษา",
      width: 200,
      headerAlign: "center",
    },
    {
      field: "name",
      headerName: "ชื่อ-นามสกุล",
      width: 250,
      headerAlign: "center",
    },
    ...days.map((day, index) => ({
      field: `checkbox_${index}`,
      headerName: formatDate(day).th,
      width: 150,
      headerAlign: "center",
      align: "center",
      sortable: false,
      disableColumnMenu: true,
      renderCell: (params) => {
        const isChecked = checkedItems[activity.act_ID]?.students[
          params.row.std_id
        ]?.includes(dateToUint32(day));
        return (
          <Checkbox
            checked={isChecked}
            onChange={(e) =>
              handleCheckboxChange(
                activity.act_ID,
                params.row.id,
                params.row.std_id,
                dateToUint32(day),
                e.target.checked
              )
            }
          />
        );
      },
    })),
  ];

  const rows = student.map((i) => ({
    id: i.login_ID,
    std_id: i.std_ID,
    name: `${i.std_fname} ${i.std_lname}`,
  }));

  return (
    <div>
      <Box sx={{ height: 400, width: "100%" }}>
        {rows.length > 0 ? (
          <DataGrid
            rows={rows}
            columns={columns}
            key={JSON.stringify(checkedItems)}
          />
        ) : (
          <p>No data available</p>
        )}
      </Box>
      <Button onClick={upload}>Upload</Button>
    </div>
  );
}
//aaa

export default Table;
