import { useContext, useState, useEffect } from "react";
import { useNavigate, useParams } from "react-router";
import axios from "axios";
import Swal from "sweetalert2";
import Button from "@mui/material/Button";
import EditNoteIcon from "@mui/icons-material/EditNote";
import ArrowBackIosNewIcon from "@mui/icons-material/ArrowBackIosNew";
import DeleteForeverIcon from "@mui/icons-material/DeleteForever";
import EditIcon from "@mui/icons-material/Edit";
import { parseISO, formatISO } from "date-fns";
import { ContextActivity } from "./ActivityContext.jsx";

function DetailActivity() {
  const [isReadOnly, setIsReadOnly] = useState(true);
  const [showButtons, setShowButtons] = useState(false);
  const [editData, setEditData] = useState({});
  const { getActivityByID, teacher } = useContext(ContextActivity);
  const [numStdReserve, setNumStdReserve] = useState("");
  const navigate = useNavigate();
  const { act_ID } = useParams();

  const role = localStorage.getItem("role");

  useEffect(() => {
    const fetchActivity = async () => {
      try {
        const activity = await getActivityByID(act_ID);

        const correctedDateStart = parseISO(activity.act_dateStart);
        const correctedDateEnd = parseISO(activity.act_dateEnd);

        setEditData({
          ...activity,
          act_dateStart: formatISO(correctedDateStart, {
            representation: "date",
          }),
          act_dateEnd: formatISO(correctedDateEnd, { representation: "date" }),
        });
      } catch (error) {
        console.error("Error fetching activity:", error);
      }
    };

    // const fetchNumStdReserve = async () => {
    //   try {
    //     const res = await axios.get(`/api/countNumStdReserve/${act_ID}`);
    //     setNumStdReserve(res.data);
    //   } catch (error) {
    //     console.log(error);
    //   }
    // };

    fetchActivity();
    // fetchNumStdReserve();
  }, [act_ID, getActivityByID]);
  console.log(editData);

  const editButton = () => {
    setIsReadOnly(!isReadOnly);
    setShowButtons(!showButtons);
  };

  const deleteActivity = async () => {
    const result = await Swal.fire({
      title: "ลบกิจกรรม?",
      text: "ยืนยันการลบกิจกรรม!",
      icon: "warning",
      showCancelButton: true,
      confirmButtonText: "ยืนยัน",
      cancelButtonText: "ยกเลิก",
      reverseButtons: true,
    });

    if (result.isConfirmed) {
      try {
        const deleteNews = {
          topic: `กิจกรรม ${editData.act_title} `,
          description: `ลบกิจกรรม ${editData.act_title} `,
        };
        await axios.delete(`/api/activitys/${act_ID}`);
        await axios.post("/api/notification", deleteNews);
        Swal.fire("ลบสำเร็จ!");
        setTimeout(() => {
          navigate(-1); // Navigate back in the history
        }, 1000);
      } catch (error) {
        console.error("Error deleting activity:", error);
        Swal.fire("Error!", "Failed to delete activity.", "error");
      }
    }
  };

  const handleStatusChange = (event) => {
    setEditData((prev) => ({ ...prev, act_status: event.target.value }));
  };

  const handleTeacherChange = (event) => {
    setEditData((prev) => ({ ...prev, t_ID: event.target.value }));
  };

  const handleInputChange = (field, value) => {
    setEditData((prev) => ({ ...prev, [field]: value }));
  };

  const handleDateChange = (field, event) => {
    const newDate = event.target.value;
    setEditData((prev) => ({
      ...prev,
      [field]: formatISO(parseISO(newDate), { representation: "date" }),
    }));
  };

  const editActivity = async () => {
    try {
      const updatedData = {
        ...editData,
        act_dateStart: formatISO(parseISO(editData.act_dateStart), {
          representation: "date",
        }),
        act_dateEnd: formatISO(parseISO(editData.act_dateEnd), {
          representation: "date",
        }),
      };

      await axios.put(`/api/activity/${act_ID}`, updatedData);

      Swal.fire("แก้ไขสำเร็จ", "แก้ไขข้อมูลกิจกรรมเรียบร้อย.", "success").then(
        () => {
          setIsReadOnly(true);
          setShowButtons(false);
        }
      );

      setTimeout(() => {
        window.location.reload();
      }, 1000);
    } catch (error) {
      console.error("Error updating activity:", error);
      Swal.fire("Error!", "Failed to update activity.", "error");
    }
  };

  const url = editData?.act_transaction
    ? `https://sepolia.etherscan.io/tx/${editData.act_transaction}`
    : "#";

  return (
    <div>
      <div className="overflow-x-auto shadow-md sm:rounded-lg bg-white p-4">
        <div className="flex justify-between items-center">
          <div className="flex gap-4 items-center">
            <h1 className="text-lg font-bold mb-2">รายละเอียดกิจกรรม</h1>
            {role == "admin" && editData.act_status != "สิ้นสุดลงแล้ว" ? (
              <Button
                onClick={editButton}
                variant="outlined"
                startIcon={<EditNoteIcon />}
                color="warning"
              >
                แก้ไข
              </Button>
            ) : null}
          </div>

          <div
            className="items-center mb-5 cursor-pointer"
            onClick={() => navigate(-1)}
          >
            <ArrowBackIosNewIcon /> ย้อนกลับ
          </div>
        </div>
        <hr className="mb-3" />
        <div className="overflow-x-auto">
          <table className="min-w-full divide-y divide-gray-200">
            <tbody className="bg-white divide-y divide-gray-200 text-md">
              <tr>
                <td className="px-6 py-4 font-medium text-gray-900">
                  ชื่อกิจกรรม
                </td>
                <td className="px-6 py-4 text-gray-500">
                  <input
                    type="text"
                    value={editData.act_title || ""}
                    readOnly={isReadOnly}
                    onChange={(e) =>
                      handleInputChange("act_title", e.target.value)
                    }
                  />
                </td>
                <td className="px-6 py-4 font-medium text-gray-900">
                  รายละเอียด
                </td>
                <td className="px-6 py-4 text-gray-500">
                  <input
                    type="text"
                    value={editData.act_desc || ""}
                    readOnly={isReadOnly}
                    onChange={(e) =>
                      handleInputChange("act_desc", e.target.value)
                    }
                  />
                </td>
              </tr>
              <tr>
                <td className="px-6 py-4 font-medium text-gray-900">สถานที่</td>
                <td className="px-6 py-4 text-gray-500">
                  <input
                    type="text"
                    value={editData.act_location || ""}
                    readOnly={isReadOnly}
                    onChange={(e) =>
                      handleInputChange("act_location", e.target.value)
                    }
                  />
                </td>
                <td className="px-6 py-4 font-medium text-gray-900">
                  จำนวนที่เปิดรับ
                </td>

                <td className="px-6 py-4 text-gray-500">
                  {editData.act_numStdReserve > 0
                    ? `${editData.act_numStdReserve} / `
                    : "Loading..."}
                  <input
                    type="text"
                    value={editData.act_numStd || ""}
                    readOnly={isReadOnly}
                    onChange={(e) =>
                      handleInputChange("act_numStd", e.target.value)
                    }
                  />
                </td>
              </tr>
              <tr>
                <td className="px-6 py-4 font-medium text-gray-900">
                  เริ่มวันที่
                </td>
                <td className="px-6 py-4 text-gray-500">
                  <input
                    type="date"
                    value={editData.act_dateStart || ""}
                    readOnly={isReadOnly}
                    onChange={(e) => handleDateChange("act_dateStart", e)}
                  />
                </td>
                <td className="px-6 py-4 font-medium text-gray-900">
                  สิ้นสุดวันที่
                </td>
                <td className="px-6 py-4 text-gray-500">
                  <input
                    type="date"
                    value={editData.act_dateEnd || ""}
                    readOnly={isReadOnly}
                    onChange={(e) => handleDateChange("act_dateEnd", e)}
                  />
                </td>
              </tr>
              <tr>
                <td className="px-6 py-4 font-medium text-gray-900">สถานะ</td>
                <td className="px-6 py-4 text-gray-500">
                  {role === "admin" ? (
                    <select
                      value={editData.act_status}
                      onChange={handleStatusChange}
                      disabled={isReadOnly}
                    >
                      <option value={"เปิดลงทะเบียน"}>เปิดลงทะเบียน</option>
                      <option value={"สิ้นสุดลงแล้ว"}>กิจกรรมจบลงแล้ว</option>
                      <option value={"ปิดลงทะเบียน"}>ปิดลงทะเบียน</option>
                    </select>
                  ) : (
                    <span>{editData.act_status}</span>
                  )}
                </td>
                <td className="px-6 py-4 font-medium text-gray-900">
                  ผู้รับผิดชอบ
                </td>
                <td className="px-6 py-4 text-gray-500">
                  {role === "admin" ? (
                    <select
                      value={editData.t_ID || ""}
                      onChange={handleTeacherChange}
                      disabled={isReadOnly}
                    >
                      <option value="">เลือกผู้รับผิดชอบ</option>
                      {teacher.map((item) => (
                        <option key={item.t_ID} value={item.t_ID}>
                          {item.t_fname} {item.t_lname}
                        </option>
                      ))}
                    </select>
                  ) : (
                    teacher.find((t) => t.t_ID === editData.t_ID)?.t_fname || ""
                  )}
                </td>
              </tr>
              {editData.act_transaction && (
                <tr>
                  <td className="px-6 py-4 font-medium text-gray-900">
                    Hash Transaction
                  </td>
                  <td className="px-6 py-4 text-gray-500" colSpan={3}>
                    <a href={url} target="_blank" rel="noopener noreferrer">
                      {editData.act_transaction}
                    </a>
                  </td>
                </tr>
              )}
            </tbody>
          </table>
        </div>
        {showButtons && (
          <div className="flex justify-center space-x-4 mt-4">
            <Button
              onClick={editActivity}
              variant="contained"
              startIcon={<EditIcon />}
              color="success"
            >
              บันทึก
            </Button>
            <Button
              onClick={deleteActivity}
              variant="contained"
              startIcon={<DeleteForeverIcon />}
              color="error"
            >
              ลบ
            </Button>
          </div>
        )}
      </div>
    </div>
  );
}

export default DetailActivity;
