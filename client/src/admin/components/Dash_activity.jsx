import Popup from "./Popup_addAc";

function Dash_users() {

  return (
    <div className="container mx-auto md:px-10 mb-5">
      <div>
        <div className="flex justify-between items-center">
          <h2 className="text-xl font-bold">การจัดการกิจกรรม</h2>
          <Popup />
        </div>
      </div>
    </div>
  );
}

export default Dash_users;
